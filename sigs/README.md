# 专项兴趣小组（Special Interest Groups，简称SIG）

* SIG工作目标为针对特定的一个或多个主题建立社区项目，并推动各项目输出交付成果。各SIG的成立都需经TC决策，并建立治理规则，明确规定该SIG的职责，内容应包括但不限于其职责范围、沟通方式、维护人员任免（Maintainer、Committer等）、业务范围（代码库、目录等）等信息。同时，各SIG内部沟通信息必须在openGauss社区范围内公开，以确保其他SIG的社区成员可以找到讨论、会议和决策等相关记录；

* 各SIG成员应包含一至两名Maintainer，多名Committer及Contributor。openGauss社区第一批SIG包括：

    | SIG名称 | 职责范围 |
    | :------- | :--------------- |
    | SQLEngine | 负责openGauss社区SQL引擎的开发和维护。 |
    | StorageEngine | 负责openGauss社区存储引擎的开发和维护。 |
    | Connectors | 负责openGauss社区Connectors的开发和维护。 |
    | Tools | 负责openGauss社区工具的开发和维护。 |
    | Docs | 负责openGauss社区文档的开发和维护。 |
    | Infra | 负责openGauss社区基础设施的开发和维护。 |
    | Security | 负责openGauss社区安全的开发和维护。 |
