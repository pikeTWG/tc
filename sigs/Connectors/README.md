***注意：本文档所有的斜体内容，请在完成时删除***

# Connectors

*负责数据库驱动的规划，开发和维护，功能范围包括JDBC，ODBC，LIBPQ，后面根据业务需求扩展其他语言驱动*


# 组织会议

- 公开的会议时间：北京时间，每周五下午，16点~17点

# 成员



### Maintainer列表

- tianwengang [@pikeTWG](https://gitee.com/pikeTWG), *tianwengang@huawei.com*


### Committer列表




# 联系方式

- [邮件列表](https://mailweb.opengauss.org/postorius/lists/connectors.opengauss.org/)



# 仓库清单


仓库地址：

- https://gitee.com/opengauss/openGauss-server
- https://gitee.com/opengauss/openGauss-third_party
- https://gitee.com/opengauss/openGauss-connector-jdbc
- https://gitee.com/opengauss/openGauss-connector-odbc

